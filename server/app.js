let express = require('express');
let mongoose = require('mongoose');
let app = express();
let server = require('http').createServer(app);
let io = require('socket.io').listen(server);
global.io = io
let Lusers = [];
let Ousers = [];

server.listen(process.env.PORT || 3000);

mongoose.connect('mongodb://abhishek:Abhi!123@cluster0-shard-00-00-xycpv.mongodb.net:27017,cluster0-shard-00-01-xycpv.mongodb.net:27017,cluster0-shard-00-02-xycpv.mongodb.net:27017/test?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin&retryWrites=true', function (err) {
  if (err) {
    console.log('error in mongoose', err);
  } else {
    console.log('Connected to mongodb!');
  }
})

let userSchema = mongoose.Schema({
  name: String,
  password: String,
  created: {type: Date, default: Date.now()},
})

let chatSchema = mongoose.Schema({
  nick: String,
  msg: String,
  created: {type: String, default: new Date().toLocaleString()},
  to: String,
  flag: Boolean,
  date: String,
  time: String,
  offline: Boolean,
  counter: Number
})

let Chat = mongoose.model('chatMessage', chatSchema);
let onlineuser = mongoose.model('chatUsers', userSchema);

let toName, fromName, badge;
let counter = 1;

io.sockets.on('connection', function (socket) {
  console.log('new connection', socket.id);
  id = socket.id;

  function notification() {
    let newdata;
    let key;
    Lusers.filter((z) => {
      let xyz = Object.keys(z);
      key = xyz[0];
      newdata = z[xyz[0]];
      Lusers[Lusers.indexOf(z)][xyz[0]][0].socket.emit('notification', newdata[0].users, newdata[0].message, newdata[0].activeUser);
    })
  }

  function updateNicknames() {
    let data;
    Lusers.filter((x) => {
      let abc = Object.keys(x);
      data = x[abc[0]];
      Lusers[Lusers.indexOf(x)][abc[0]][0].socket.emit('usernames', data[0].users, data[0].socket.nickname);
    });
  }

  socket.on('getOnlineUser', function (callback) {
    let query1 = onlineuser.find();
    query1.sort('-created').exec(function (err, onlinUser) {
      if (err) {
      } else {
        callback(onlinUser)
      }
    })
  })

  socket.on('new user', function (data, callback) {
    let query1 = onlineuser.find();
    query1.sort('-created').exec(function (err, onlinUser) {
      if (err) throw err;
      let keys = [];
      onlinUser.filter((x) => {
        if (Ousers.length !== onlinUser.length)
          Ousers.push({name: x.name, password: x.password});
      })
      Ousers.filter((y) => {
        if (keys.indexOf(y.name) === -1)
          keys.push(y.name);
      })
      let check = false;
      Lusers.filter((x) => {
        if (Object.keys(x)[0] === data[0]) {
          check = true;
        }
      })
      if (keys.indexOf(data[0]) !== -1 && check !== true) {
        if (Ousers[keys.indexOf(data[0])].password === data[1]) {
          callback(true, '');
          socket.nickname = data[0];
          if (Lusers.length > 0) {
            let newKey = [];
            Lusers.filter((y) => {
              let key = Object.keys(y);
              newKey.push(key[0]);
              (y[key[0]][0].users).push(socket.nickname);
            });
            let abc = {};
            abc[socket.nickname] = [{socket: socket, users: newKey}];
            Lusers.push(abc);
          } else {
            let abc = {};
            abc[socket.nickname] = [{socket: socket, users: []}];
            Lusers.push(abc);
          }
          updateNicknames();
        } else {
          callback(false, 'Username/Password Wrong! Try again.');
        }
      } else {
        if (check === true) {
          callback(false, 'You are already Login into system');
        } else {
          callback(false, 'You are not Register User to use this app');
        }
      }
    })
  })

  socket.on('findLastMessage', function (name, cb) {
    let query = Chat.aggregate([
      {
        "$match": {
          "to": name
        }
      },
      {
        "$sort": {
          "created": -1
        }
      },
      {
        "$group": {
          "_id": "$nick",
          "msg": {
            "$first": "$msg"
          },
          "created": {
            "$first": "$created"
          },
          "offline": {
            "$first": "$offline"
          },
          "counter": {
            "$first": "$counter"
          },
        }
      },
      {
        "$project": {
          "_id": 0,
          "nick": "$_id",
          "msg": 1,
          "created": -1,
          "offline": 1,
          "counter": 1
        }
      }
    ])
    query.exec(function (err, docs) {
      if (err)
        cb(err, null)
      else
        docs.filter((x) => {
          x['date'] = formatDate(x.created);
          x['time'] = formatTime(x.created);
        })
      cb(null, docs)
    })
  })

  socket.on('find message', function (name) {
    console.log('find', socket.nickname, name)
    let newArr = [];
    Lusers.filter((x) => {
      newArr.push(Object.keys(x)[0]);
    })
    let keys = [];
    if (Lusers[newArr.indexOf(socket.nickname)][socket.nickname][0].message) {
      let len = (Lusers[newArr.indexOf(socket.nickname)][socket.nickname][0].message).length
      for (let i = 0; i < len; i++) {
        keys.push(Object.keys(Lusers[newArr.indexOf(socket.nickname)][socket.nickname][0].message[i])[0])
      }
      if (keys.indexOf(name) !== -1) {
        let abc = {};
        abc[name] = {
          read: 1,
          counter: 0,
        }
        Lusers[newArr.indexOf(socket.nickname)][socket.nickname][0].message[keys.indexOf(name)] = abc
      }
    }
    Lusers[newArr.indexOf(socket.nickname)][socket.nickname][0].activeUser = [];
    let abc = {}
    abc[name] = true;
    abc['id'] = Lusers[newArr.indexOf(socket.nickname)][socket.nickname][0].socket.id;
    Lusers[newArr.indexOf(socket.nickname)][socket.nickname][0].activeUser[0] = abc;

    let query = Chat.find({$or: [{$and: [{nick: socket.nickname}, {to: name}]}, {$and: [{nick: name}, {to: socket.nickname}]}]});
    query.sort('-created').exec(function (err, docs) {
      if (err) throw err;
      docs.filter((x) => {
        if ((socket.nickname) === x.to) {
          x['flag'] = true
        } else {
          x['flag'] = false;
        }
        x['date'] = formatDate(x.created);
        x['time'] = formatTime(x.created);
      })
      notification();
      socket.emit('load old msgs', docs);
    })
  })

  socket.on('send message', function (newData, callback) {
    let currentDate = newData[1];
    let data = newData[0];
    console.log('curdate', currentDate);
    console.log('date', new Date());
    console.log('date1', new Date());
    let msg = data.trim();
    if (msg.substr(0, 3) === "/w ") {
      msg = msg.substr(3);
      var ind = msg.indexOf(' ');
      if (ind !== -1) {
        var name = msg.substring(0, ind).trim();
        msg = msg.substring(ind + 1);
        let newArr = [];
        Lusers.filter((x) => {
          newArr.push(Object.keys(x)[0]);
        })
        if (newArr.indexOf(name) !== -1) {
          let flag = (socket.nickname === name) ? true : false;
          let newMsg = new Chat({
            msg: msg,
            nick: socket.nickname,
            to: name,
            created: currentDate,
            offline: false,
            counter: 0
          });
          newMsg.save(function (err) {
            if (err) throw err;

            // Lusers[newArr.indexOf(socket.nickname)][socket.nickname][0].socket.emit('new message', {
            //   msg: msg,
            //   nick: socket.nickname,
            //   flag: flag,
            //   date: formatDate(currentDate),
            //   time: formatTime(currentDate),
            // })
            console.log('see', Lusers[newArr.indexOf(name)][name][0].activeUser);
            console.log('my', Lusers[newArr.indexOf(socket.nickname)][socket.nickname][0].socket.id);
            console.log('samane wala', Lusers[newArr.indexOf(name)][name][0].socket.id);

            if (Lusers[newArr.indexOf(name)][name][0].activeUser === undefined || Object.keys(Lusers[newArr.indexOf(name)][name][0].activeUser[0])[0] !== socket.nickname) {
              if (Lusers[newArr.indexOf(name)][name][0].message === undefined) {
                Lusers[newArr.indexOf(name)][name][0].message = [];
              }
              let keys = [];
              let len = (Lusers[newArr.indexOf(name)][name][0].message).length
              for (let i = 0; i < len; i++) {
                keys.push(Object.keys(Lusers[newArr.indexOf(name)][name][0].message[i])[0])
              }
              let arr = Lusers[newArr.indexOf(name)][name][0].message;
              if (keys.indexOf(socket.nickname) !== -1) {
                let abc = {};
                abc[socket.nickname] = {
                  read: 0,
                  counter: (arr[keys.indexOf(socket.nickname)][socket.nickname].counter) + 1,
                }
                Lusers[newArr.indexOf(name)][name][0].message[keys.indexOf(socket.nickname)] = abc;
              } else {
                let abc = {};
                abc[socket.nickname] = {
                  read: 0,
                  counter: counter,
                }
                Lusers[newArr.indexOf(name)][name][0].message.push(abc);
              }

              toName = Lusers[newArr.indexOf(name)][name][0].socket.id;
              fromName = socket.nickname;
              badge = true;
              notification();
            } else {
              Lusers[newArr.indexOf(name)][name][0].socket.emit('new message', {
                msg: msg,
                nick: socket.nickname,
                flag: !(flag),
                date: formatDate(currentDate),
                time: formatTime(currentDate),
              })
            }
          })
          console.log('whisper!')
        } else {
          let newcounter;
          console.log('see for offline msg', Lusers[newArr.indexOf(socket.nickname)][socket.nickname][0].offline);
          if (Lusers[newArr.indexOf(socket.nickname)][socket.nickname][0].offline) {
            console.log('1');
            if (Lusers[newArr.indexOf(socket.nickname)][socket.nickname][0].offline[name]) {
              console.log('2');
              if (Lusers[newArr.indexOf(socket.nickname)][socket.nickname][0].offline[name].read === 0) {
                console.log('3', Lusers[newArr.indexOf(socket.nickname)][socket.nickname][0].offline[name].newcounter);
                newcounter = (Lusers[newArr.indexOf(socket.nickname)][socket.nickname][0].offline[name].newcounter) + 1;
                Lusers[newArr.indexOf(socket.nickname)][socket.nickname][0].offline[name].newcounter = newcounter;
              } else {
                console.log('4');
                newcounter = 1;
                Lusers[newArr.indexOf(socket.nickname)][socket.nickname][0].offline[name].newcounter = 1;
              }
            } else {
              console.log('5');
              Lusers[newArr.indexOf(socket.nickname)][socket.nickname][0].offline[name] = {
                read: 0,
                newcounter: 1
              }
              newcounter = 1;
              console.log('chat', Lusers[newArr.indexOf(socket.nickname)][socket.nickname][0].offline[name]);
            }
          } else {
            console.log('6');
            Lusers[newArr.indexOf(socket.nickname)][socket.nickname][0].offline = {};
            Lusers[newArr.indexOf(socket.nickname)][socket.nickname][0].offline[name] = {
              read: 0,
              newcounter: 1
            }
            newcounter = 1;
            console.log('chat', Lusers[newArr.indexOf(socket.nickname)][socket.nickname][0].offline);
          }
          let newMsg = new Chat({
            msg: msg,
            nick: socket.nickname,
            to: name,
            created: currentDate,
            offline: true,
            counter: newcounter
          });
          newMsg.save(function (err) {
            if (err) throw err;
          })
        }

      } else {
        callback('Error! Please Enter a message for your whisper')
      }

    }
  })

  socket.on('disconnect', function (data) {
    if (!socket.nickname) return;
    let newdata;
    Lusers.filter((x) => {
      let abc = Object.keys(x);
      if (socket.nickname === abc[0]) {
        delete Lusers[Lusers.indexOf(x)]
      } else {
        data = x[abc[0]];
        (data[0].users).filter((x) => {
          var index = data[0].users.indexOf(socket.nickname);
          if (index > -1) {
            (data[0].users).splice(index, 1);
          }
        })
        Lusers[Lusers.indexOf(x)][abc[0]][0].socket.emit('usernames', data[0].users);
      }
    })
    updateNicknames();
  })

})


function formatDate(date) {
  date = new Date(date);
  var monthNames = [
    "January", "February", "March",
    "April", "May", "June", "July",
    "August", "September", "October",
    "November", "December"
  ];

  var day = date.getDate();
  var monthIndex = date.getMonth();
  var year = date.getFullYear();

  return monthNames[monthIndex] + ' ' + day;
}

function formatTime(date) {
  date = new Date(date);
  var hours = date.getHours();
  var minutes = date.getMinutes();
  var ampm = hours >= 12 ? 'pm' : 'am';
  hours = hours % 12;
  hours = hours ? hours : 12; // the hour '0' should be '12'
  minutes = minutes < 10 ? '0' + minutes : minutes;
  var strTime = hours + ':' + minutes + ' ' + ampm;
  return strTime;
}
