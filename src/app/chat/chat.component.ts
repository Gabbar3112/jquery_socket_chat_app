import {Component, OnInit} from '@angular/core';
import {UserService} from '../Services/user.service';
import {Socket} from 'ngx-socket-io';
import {SharedService} from '../Services/shared.service';
import {ToasterConfig, ToasterService} from 'angular2-toaster';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent implements OnInit {
  username;
  showChat = false;
  selectUser;
  msg = [];
  msgval;
  counters = [];
  myKey = [];
  chatList = [];
  listofuser = [];

  constructor(private userService: UserService,
              private socket: Socket,
              private sharedService: SharedService,
              private toasterService: ToasterService) {
  }

  public config: ToasterConfig =
    new ToasterConfig({
      showCloseButton: true,
      tapToDismiss: true,
      timeout: 3000,
      positionClass: 'toast-top-right'
    });

  ngOnInit() {
    const that = this;

    this.userService.getonlineusers(this.socket, function (data) {
      that.listofuser = [];
      // local online user : data[0]
      that.username = data[1];
      that.userService.findLastMessage(that.username, function (err, messages) {
        if ((that.chatList).length <= 0) {
          that.chatList = messages;
        } else {
          (that.chatList).filter((x) => {
            (messages).filter((y) => {
              if ((that.chatList).indexOf(y.nick) !== -1) {
                that.chatList.push(y);
              }
            });
          });
        }
        console.log('message', that.chatList);
        that.userService.getOnlineUserList(function (newdata) {
          const key = [];
          if ((that.chatList).length > 0) {
            (that.chatList).filter((y) => {
              if (y.offline === true) {
                that.myKey[y.nick] = y.counter;
              }
              key.push(y.nick);
            });
          }
          newdata.filter((x) => {
            if (x.name !== that.username && key.indexOf(x.name) === -1) {
              that.listofuser.push(x.name);
            }
          });
        });
      });
    });
    this.userService.newmessage(function (msgdata) {
      that.msg.push(msgdata);
    });
    this.userService.notification(function (notificationdata) {
      console.log('notificationdata', notificationdata);

      if (notificationdata[1] !== null) {
        (notificationdata[1]).filter((x) => {
          console.log('x', x);
          const key = Object.keys(x);
          if (x[key[0]].read === 0) {
            that.toasterService.pop('info', 'New Message from ' + key[0]);
            console.log('seee', key[0], that.counters.indexOf(key[0]));
            that.myKey[key[0]] = x[key[0]].counter;
          } else {
            delete that.myKey[key[0]];
          }
        });
        console.log('seee here', that.myKey);
        // console.log('seee here', that.counters.length);
      }
    });

  }

  selectedUser(user) {
    this.selectUser = user;
    this.showChat = true;
    const that = this;
    this.userService.findMessage(user, function (data) {
      (data[1]).sort((a, b) => new Date(a.created).getTime() - new Date(b.created).getTime());
      that.msg = [];
      that.msg = data[1];
    });
  }

  openMessagebox(name) {
    this.selectUser = name;
    this.showChat = true;
    const currentDate = new Date().toLocaleString();
    const data = {
      msg: '',
      nick: name,
      date: this.formatDate(currentDate),
      time: this.formatTime(currentDate),
    };
    const index = (this.listofuser).indexOf(name);
    if (index > -1) {
      (this.listofuser).splice(index, 1);
    }
    this.chatList.push(data);
    this.array_move(this.chatList, ((this.chatList).length - 1), 0);
    this.msg = [];
    this.selectedUser(name);
  }

  sendmessage() {
    const that = this;
    const strng = '/w ' + this.selectUser + ' ' + this.msgval;
    console.log('send message', strng);
    const currentDate = new Date().toLocaleString();
    if ((this.msgval).length > 0) {
      this.userService.sendMessage(strng, currentDate, function (err) {
        if (err) {
          that.toasterService.pop('error', err);
        }
      });
      const data = {
        msg: this.msgval,
        nick: this.username,
        flag: false,
        date: that.formatDate(currentDate),
        time: that.formatTime(currentDate),
      };
      this.msg.push(data);
      data['nick'] = this.selectUser;
      (this.chatList).filter((x) => {
        if (x.nick === this.selectUser) {
          that.chatList[(that.chatList).indexOf(x)].msg = data.msg;
          that.chatList[(that.chatList).indexOf(x)].date = data.date;
          that.chatList[(that.chatList).indexOf(x)].time = data.time;
          that.array_move(this.chatList, (this.chatList).indexOf(x), 0);
        }
      });
      console.log('chatlist', this.chatList);
    }
    this.msgval = '';
  }

  showMessage() {
    const that = this;

  }

  array_move(arr, old_index, new_index) {
    if (new_index >= arr.length) {
      let k = new_index - arr.length + 1;
      while (k--) {
        arr.push(undefined);
      }
    }
    arr.splice(new_index, 0, arr.splice(old_index, 1)[0]);
    return arr; // for testing
  }

  formatDate(date) {
    date = new Date(date);
    const monthNames = [
      'January', 'February', 'March',
      'April', 'May', 'June', 'July',
      'August', 'September', 'October',
      'November', 'December'
    ];

    const day = date.getDate();
    const monthIndex = date.getMonth();
    const year = date.getFullYear();

    return monthNames[monthIndex] + ' ' + day;
  }

  formatTime(date) {
    date = new Date(date);
    let hours = date.getHours();
    let minutes = date.getMinutes();
    const ampm = hours >= 12 ? 'pm' : 'am';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0' + minutes : minutes;
    const strTime = hours + ':' + minutes + ' ' + ampm;
    return strTime;
  }

}
