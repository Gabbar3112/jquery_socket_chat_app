import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { LoginpageComponent } from './loginpage/loginpage.component';
import {FormsModule} from '@angular/forms';
import {AppRoutingModule} from './app-routing.module';
import {ToasterModule, ToasterService} from 'angular2-toaster';
import {UserService} from './Services/user.service';
import {NgxUiLoaderModule} from 'ngx-ui-loader';
import {HttpClientModule} from '@angular/common/http';
import { SocketIoModule, SocketIoConfig } from 'ngx-socket-io';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { ChatComponent } from './chat/chat.component';

const config: SocketIoConfig = { url: 'https://personalchatappserver.herokuapp.com/', options: {} };

@NgModule({
  declarations: [
    AppComponent,
    LoginpageComponent,
    ChatComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    AppRoutingModule,
    ToasterModule.forRoot(),
    NgxUiLoaderModule,
    HttpClientModule,
    SocketIoModule.forRoot(config)
  ],


  providers: [UserService, ToasterService],
  bootstrap: [AppComponent]
})
export class AppModule { }
