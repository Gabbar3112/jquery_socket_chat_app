import {Component, OnInit, ViewChild, ElementRef} from '@angular/core';

import {Router} from '@angular/router';
import {UserService} from '../Services/user.service';
import {ToasterService, ToasterConfig} from 'angular2-toaster';
import {NgxUiLoaderService} from 'ngx-ui-loader';
import {SharedService} from '../Services/shared.service';
import JsBarcode from 'jsbarcode';


@Component({
  selector: 'app-loginpage',
  templateUrl: './loginpage.component.html',
  styleUrls: ['./loginpage.component.css']
})

export class LoginpageComponent implements OnInit {
  model: any = {};
  @ViewChild('barcode') barcode: ElementRef;

  constructor(private userService: UserService,
              private router: Router,
              private toasterService: ToasterService,
              private ngxService: NgxUiLoaderService) {
  }

  public config: ToasterConfig =
    new ToasterConfig({
      showCloseButton: true,
      tapToDismiss: true,
      timeout: 3,
      positionClass: 'toast-top-right'
    });

  ngOnInit() {
    JsBarcode(this.barcode.nativeElement, 'Abhishek');
    const socket = this.userService.newconnection();
  }

  login() {
    this.ngxService.start();

    console.log('model', this.model);
    const that = this;
    this.userService.login(this.model.uname, this.model.password, function (err, data) {
      if (err) {
        that.ngxService.stop();
        that.toasterService.pop('error', err);
      } else {
        that.ngxService.stop();
        that.router.navigate(['/chat']);
      }
    });

  }

}
