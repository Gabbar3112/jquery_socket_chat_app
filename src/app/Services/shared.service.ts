import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SharedService {

  username;
  onlineusers;

  constructor() {
  }

  setValue(data) {
    this.username = data[2];
    this.onlineusers = data[1];
  }

  getValue() {
    return [this.username, this.onlineusers];
  }
}
